import {CSFD} from './CSFD';
import {CSFDItem} from './CSFDItem';
import {CSFDItemProps, CSFDSearchResponse} from './types';

export class CSFDSearchItem {
    private csfd: CSFD;
    _response: CSFDSearchResponse;
    _items: CSFDItem[] = [];

    get items() {
        return this._items;
    }

    get response() {
        return this._response;
    }

    constructor(csfd: CSFD, res: CSFDSearchResponse) {
        this.csfd = csfd;
        this._response = res;
        this._items = [...res.films, ...res.series].map(i => this.csfd.get(i.link.match(/\/.*csfd\.cz.*\/(\d+)|^[0-9]+$/)?.pop() as string));
    }

    fetchAll(children = true) {
        return this.csfd.fetcher.chunkedPromise<CSFDItem, CSFDItemProps>(this.items, async (child) => {
            return child.fetch(children);
        });
    }
}
