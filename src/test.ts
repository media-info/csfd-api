import {CSFD} from './CSFD';

const csfd = new CSFD('cs', {
    proxyUrl: '',
    retry: 10,
    debug: true,
});

(async () => {
    const data = await csfd.fetch(857993);
    console.log(data);
    console.log('done');
})().catch(e => console.error(e));
